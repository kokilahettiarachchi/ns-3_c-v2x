/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
  This software was developed at the National Institute of Standards and
  Technology by employees of the Federal Government in the course of
  their official duties. Pursuant to titleElement 17 Section 105 of the United
  States Code this software is not subject to copyright protection and
  is in the public domain.
  NIST assumes no responsibility whatsoever for its use by other parties,
  and makes no guarantees, expressed or implied, about its quality,
  reliability, or any other characteristic.

  We would appreciate acknowledgement if the software is used.

  NIST ALLOWS FREE USE OF THIS SOFTWARE IN ITS "AS IS" CONDITION AND
  DISCLAIM ANY LIABILITY OF ANY KIND FOR ANY DAMAGES WHATSOEVER RESULTING
  FROM THE USE OF THIS SOFTWARE.

 * Modified by: Fabian Eckermann <fabian.eckermann@udo.edu> (CNI)
 *              Moritz Kahlert <moritz.kahlert@udo.edu> (CNI)
 */

//parameter values : ETSI TS 103 613

#include "ns3/lte-helper.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/lte-v2x-helper.h"
#include "ns3/config-store.h"
#include "ns3/lte-hex-grid-enb-topology-helper.h"
#include <ns3/buildings-helper.h>
#include <ns3/cni-urbanmicrocell-propagation-loss-model.h>
#include <ns3/constant-position-mobility-model.h>
#include <ns3/spectrum-analyzer-helper.h>
#include <ns3/multi-model-spectrum-channel.h>
#include "ns3/ns2-mobility-helper.h"
#include <cfloat>
#include <sstream>
#include <queue>



using namespace ns3;

NS_LOG_COMPONENT_DEFINE("v2x_communication_mode_4");

// Output 
std::string simtime = "log_simtime_v2x.csv";
std::string rx_data = "log_rx_data_v2x.csv";
std::string tx_data = "log_tx_data_v2x.csv";
//std::string connections = "log_connections_v2x.csv";
//std::string positions = "log_positions_v2x.csv"; 

//Ptr<OutputStreamWrapper> log_connections;
Ptr<OutputStreamWrapper> log_simtime;
//Ptr<OutputStreamWrapper> log_positions;;
Ptr<OutputStreamWrapper> log_rx_data;
Ptr<OutputStreamWrapper> log_tx_data;



// Global variables
uint32_t ctr_totRx = 0; // Counter for total received packets
uint32_t ctr_totTx = 0; // Counter for total transmitted packets
uint32_t ctr_qdrop = 0; // Counter for drops due to queue full 
uint32_t ctr_all_generated_packets = 0; // number of packets generating before the queue
uint16_t lenCam;
double baseline = 150.0; // Baseline distance in meter (150m for urban, 320m for freeway)
uint16_t maxQueueLength = 10;



// Responders users 
NodeContainer ueVeh;

/*void SidelinkV2xMonitoringTrace (Ptr<OutputStreamWrapper> stream)
{
 *stream->GetStream () << Simulator::Now ().GetSeconds () << std::endl;
}

void SidelinkV2xAnnouncementPhyTrace (Ptr<OutputStreamWrapper> stream)
{
 *stream->GetStream () << Simulator::Now ().GetSeconds () << std::endl;
}

void SidelinkV2xAnnouncementMacTrace (Ptr<OutputStreamWrapper> stream)
{
 *stream->GetStream () << Simulator::Now ().GetSeconds () << std::endl;
}*/


//void push_queue(int element) // Function used to insert an element into the queue
//{
//    int element;
//    if (queue_rear == LIMIT - 1)
//        cout << "Queue Overflow\n";
//    else {
//        if (queue_front == -1)
//            queue_front = 0;
//        cout << "Enter the element to be inserted in the queue: ";
//        queue_rear++;
//        packet_queue[queue_rear] = element;
//    }
//}
//
//int pop_queue() { // Function used to get the end element from the queue
//    {
//        if (queue_front == -1 || queue_front > queue_rear) {
//            return void;
//        } else {
//            //cout<<"The deleted element in the queue is: "<< packet_queue[queue_front]<<endl;
//            int poped_element = packet_queue[queue_front];
//            queue_front++;
//            return poped_element;
//        }
//    }
//}


typedef std::pair<double, std::string> packet_queue_item;
//typedef std::vector<packet_queue_item> packet_queue;



//  template<typename _Tp>
//    struct greater_pkt : public binary_function<_Tp, _Tp, bool>
//    {
//      _GLIBCXX14_CONSTEXPR
//      bool
//      operator()(const _Tp& __x, const _Tp& __y) const
//      { return __x.first  __y.first; }
//    };

//bool compare_paktq_item(packet_queue_item a, packet_queue_item b) {
//    return (a.first < b.first);
//}

//bool operator<(const packet_queue_item& a, const packet_queue_item& b) {
//    return a.first < b.first;
//}

//std::queue<packet_queue_item> time_cam_vec;

//all cam packet sequences for all nodes: a map of nodeid:cam_packet_sequence pairs
std::map<int, std::queue<packet_queue_item>> packet_map;
std::map<int, std::queue<packet_queue_item>> denm_packet_map;
std::map<int, std::queue<packet_queue_item>> cam_packet_map;


std::map<int, std::queue<packet_queue_item>> dynamic_pkt_map;




//cam packet interval must be larger than RRI

std::queue<packet_queue_item> generate_cam_sequence(uint32_t packet_interval_ms, uint32_t sim_duration_s) {
    uint32_t time_ms = 3000; //packet generation will start after fist 3 seconds, 
    //because it take about 2 seconds to initialize the system

    int packet_id = 1;
    //    double interval = 1000.0 * (1.0 / rate_per_s);
    std::queue<packet_queue_item> time_cam_queue;

    while (time_ms < sim_duration_s * 1000.0) {
        //        NS_LOG_INFO(packet_id);
        //        NS_LOG_INFO(time_ms);
        std::string cam_packet = "CAM " + std::to_string(packet_id) + " Generated on : " + std::to_string(time_ms) + "ms";
        ctr_all_generated_packets += ueVeh.GetN() - 1; //this must be changed if non constant positions and larger areas
        time_cam_queue.push({time_ms, cam_packet});
        packet_id++;
        time_ms += packet_interval_ms;

    };

    //    time_cam_vec = {
    //        {0, "first cam packet"},
    //        {500, "2nd cam packet"},
    //        {1000, "3rd cam packet"},
    //        {1500, "4th cam packet"}
    //    };
    return time_cam_queue;
}



// here K is number of repetitions + 1 (number of all similar pakets transmitted)
// lambda is the exponential distribution 1/mean

std::queue<packet_queue_item> generate_denm_sequence(uint32_t repetition_interval_ms, uint8_t K, double lambda, uint32_t sim_duration_s) {
    std::queue<packet_queue_item> time_denm_queue;

    double bound = 0;
    double mean = double(1.0 / lambda);
    uint32_t time_ms = 3000; //because of the same reason mentioned in cam generator function
    uint16_t random_value; //distributed exponentially with lambda (SD=Mean=1/lambda)   
    uint16_t packet_id = 1;

    //randomize with time to generate different sequences in different iterations
    ns3::RngSeedManager::SetSeed(time(NULL));

    Ptr<ExponentialRandomVariable> x_H = CreateObject<ExponentialRandomVariable> ();
    x_H->SetAttribute("Mean", DoubleValue(mean));
    x_H->SetAttribute("Bound", DoubleValue(bound));
    NS_LOG_INFO("denm mean " << mean);
    uint32_t sim_duration_ms = sim_duration_s * 1000;
    while (time_ms < sim_duration_ms) {
        random_value = x_H->GetValue()*1000;
        time_ms = time_ms + random_value;
        if (time_ms > sim_duration_ms) break;
        std::string denm_packet = "DENM " + std::to_string(packet_id) + " Generated on : " + std::to_string(time_ms) + "ms";
        time_denm_queue.push({time_ms, denm_packet});
        ctr_all_generated_packets += ueVeh.GetN() - 1; //this must be changed if non constant positions and larger areas

        packet_id++;
        //repeating:
        for (uint8_t itr_k = 1; itr_k < K; itr_k++) { //itr_k is started from 1 because already sent the original packet. Thease are duplicates for reliability
            time_ms = time_ms + repetition_interval_ms;
            if (time_ms > sim_duration_ms) break;
            //NS_LOG_INFO(packet_id);
            //NS_LOG_INFO(time_ms);
            std::string denm_packet = "DENM " + std::to_string(packet_id) + " Generated(Repeat) on : " + std::to_string(time_ms) + "ms";
            time_denm_queue.push({time_ms, denm_packet});
            ctr_all_generated_packets += ueVeh.GetN() - 1; //this must be changed if non constant positions and larger areas
            packet_id++;
        }
    };
    return time_denm_queue;
}

void print_packet_seq(std::queue<packet_queue_item> time_cam_vec) {
    //    std::cout << "vector = { ";
    //    for (packet_queue_item p : time_cam_vec) {
    //        //        std::cout << p << ", ";
    //        std::cout << p.first << ", " << p.second << std::endl;
    //    }
    //    std::cout << "}; \n";

    std::queue<packet_queue_item> g = time_cam_vec;
    while (!g.empty()) {
        packet_queue_item p = g.front();
        std::cout << p.first << ", " << p.second << std::endl;
        g.pop();
    }
    std::cout << '\n';
}

void print_packet_map(std::map<int, std::queue<packet_queue_item>> packet_vector) {
    for (auto p : packet_vector) {
        std::cout << "Node Id: " << p.first << ", \nPacket Sequence:" << std::endl;
        print_packet_seq(p.second);
    }
}

packet_queue_item queue_get_first(std::queue<packet_queue_item> *time_cam_vec) {
    packet_queue_item p = time_cam_vec->front();
    //    std::pair<double, std::string> p = time_cam_vec.begin();
    //    std::cout << p.first << ", " << p.second << std::endl;
    std::cout << p.first << " " << p.second << "\n";
    time_cam_vec->pop();
    return p;
}


// function to merge two queues and return final queue

std::queue<packet_queue_item> merge_queues(std::queue<packet_queue_item> q1, std::queue<packet_queue_item> q2) {
    std::queue<packet_queue_item> merged_queue;
    //    std::queue<packet_queue_item> merged_queue_sorted;

    // inserting in final queue
    while (!q1.empty() || !q2.empty()) {
        // inserting element of q1 if q1 is non-empty
        //assuming that both queues are already sorted
        if (!q1.empty() && !q2.empty()) {
            if (q1.front().first < q2.front().first) {
                merged_queue.push(q1.front());
                q1.pop();
            } else if (q1.front().first > q2.front().first) {
                merged_queue.push(q2.front());
                q2.pop();
            } else {
                merged_queue.push(q2.front());
                q2.pop();
                merged_queue.push(q1.front());
                q1.pop();
            }
        } else if (q1.empty()) {
            merged_queue.push(q2.front());
            q2.pop();
        } else if (q2.empty()) {
            merged_queue.push(q1.front());
            q1.pop();
        }

        //        if (!q1.empty()) {
        //            std::cout << q1.front().first << std::endl;
        //            merged_queue.push(q1.front());
        //            q1.pop();
        //        }
        //        // inserting element of q2 if q2 is non-empty
        //        if (!q2.empty()) {
        //            merged_queue.push(q2.front());
        //            q2.pop();
        //        }
    }

    //    std::dequeue<packet_queue_item> merged_queue_sorted;
    //    
    //    for (int i = 0; i < numProcesses; i++) {
    //        readyQueue.push_back(aProcess);
    //    }
    //    std::sort(std::begin(merged_queue), std::end(merged_queue));

    //    std::sort(std::begin(merged_queue), std::end(merged_queue), compare_paktq_item);
    //    sort(arr, arr + n, std::greater<int>());

    return merged_queue;
}

void
PrintStatus(uint32_t s_period, Ptr<OutputStreamWrapper> log_simtime) {
    if (ctr_totRx > ctr_totTx) {
        ctr_totRx = ctr_totTx;
    }
    *log_simtime->GetStream() << Simulator::Now().GetMicroSeconds() << ";" << ctr_totRx << ";" << ctr_totTx << ";" << (double) ctr_totRx / ctr_totTx << std::endl;
    std::cout << "t=" << Simulator::Now().GetMicroSeconds() << "\t Rx/Tx=" << ctr_totRx << "/" << ctr_totTx << "\t PRR=" << (double) ctr_totRx / ctr_totTx << std::endl;
    Simulator::Schedule(Seconds(s_period), &PrintStatus, s_period, log_simtime);

}

//void dropQueueLengthExceededPackets(int node_id,uint32_t simTime) {
//    uint16_t queue_length=10;
//    uint32_t itrTime=0;
//    
//    do{
//        
//    }while(itrTime<simTime);
//}

void
SidelinkV2xAnnouncementMacTrace(Ptr<Socket> socket) {
    //    NS_LOG_INFO("SidelinkV2xAnnouncementMacTrace has been invoked!!!");
    Ptr <Node> node = socket->GetNode();
    int id = node->GetId();
    uint32_t simTime = Simulator::Now().GetMicroSeconds();
    Ptr<MobilityModel> posMobility = node->GetObject<MobilityModel>();
    Vector posTx = posMobility->GetPosition();


    NS_LOG_INFO(simTime << " Got CSR for node " << id);
    bool have_packet_to_send = false;

    packet_queue_item p;
    //    if (!denm_packet_map[id].empty()) {
    //        if (denm_packet_map[id].front().first * 1000 <= simTime) {
    //            have_packet_to_send = true;
    //            p = denm_packet_map[id].front();
    //            denm_packet_map[id].pop();
    //            //            NS_LOG_INFO("Popped: " << p.second << " at " << simTime/1000 << "ms \n");
    //        }
    //    }
    //    if (!have_packet_to_send && !cam_packet_map[id].empty()) { //no denm to send
    //        if (cam_packet_map[id].front().first * 1000 <= simTime) {
    //            have_packet_to_send = true;
    //            p = cam_packet_map[id].front();
    //            cam_packet_map[id].pop();
    //            //            NS_LOG_INFO("Popped: " << p.second << " at " << simTime/1000 << "ms \n");
    //        }
    //    }





    uint16_t currentQueueLength = dynamic_pkt_map[id].size();



//    print_packet_map(dynamic_pkt_map);


    //        if (!packet_map[id].empty()) { //have pkts
    //             NS_LOG_INFO("test-3\n");
    while ( (!packet_map[id].empty()) && (packet_map[id].front().first * 1000 <= simTime) ) {
        NS_LOG_INFO("queue length=" << currentQueueLength);
        p = packet_map[id].front();
        //                NS_LOG_INFO("test-2_2\n");
//        NS_LOG_INFO(simTime << " lower time " << p.second << "\n");
        //                
        packet_map[id].pop();
        //                NS_LOG_INFO("test-1\n");
        if (currentQueueLength < maxQueueLength) {
            dynamic_pkt_map[id].push(p);
            currentQueueLength++;
        } else {
            NS_LOG_INFO("Dropped due to queue length limit: " << p.second);
            ctr_qdrop++;
        }
    }
    //        }


    //    NS_LOG_INFO("test0\n");
    if (!dynamic_pkt_map[id].empty()) { //have pkt to send
        //        if (dynamic_pkt_map[id].front().first * 1000 <= simTime) {
        have_packet_to_send = true;
        //        NS_LOG_INFO("test1\n");
        p = dynamic_pkt_map[id].front();
        //        NS_LOG_INFO("test2\n");
        dynamic_pkt_map[id].pop();
//        NS_LOG_INFO("Popped: " << p.second << " at " << simTime / 1000 << "ms\n");
        //        }
    }

    //    if (!packet_map[id].empty()) { //have pkt to send
    //        if (packet_map[id].front().first * 1000 <= simTime) {
    //            have_packet_to_send = true;
    //            p = packet_map[id].front();
    //            packet_map[id].pop();
    //            NS_LOG_INFO("Popped: " << p.second << " at " << simTime / 1000 << "ms\n");
    //        }
    //    }

    if (have_packet_to_send) {

        uint32_t packet_time_ms = p.first;
        //        NS_LOG_INFO("Packet time ms: " << packet_time_ms << " simTime: " << simTime / 1000);
        //            queuing_delay = simTime - packet_time_ms;
        NS_LOG_INFO("Sending packet simTime/ms: " << simTime / 1000 << " Node: " << id << " Packet: " << p.second); // << "\n"

        // check for each UE distance to transmitter
        for (uint8_t i = 0; i < ueVeh.GetN(); i++) {
            Ptr<MobilityModel> mob = ueVeh.Get(i)->GetObject<MobilityModel>();
            Vector posRx = mob->GetPosition();

            double distance = sqrt(pow((posTx.x - posRx.x), 2.0) + pow((posTx.y - posRx.y), 2.0));
            if (distance > 0 && distance <= baseline) {
                ctr_totTx++; //Counter for total transmitted packets. this value is used for calculation of PRR
            }
        }
        // Generate packet 
        std::ostringstream msg;
        //replaced simTime from packet_time_ms to add queuing delay
        msg << id - 1 << ";" << simTime << ";" << (int) posTx.x << ";" << (int) posTx.y << ";" << packet_time_ms << ";" << p.second << '\0'; //for rx log
        Ptr<Packet> packet = Create<Packet>((uint8_t*) msg.str().c_str(), lenCam);
        socket->Send(packet);
        *log_tx_data->GetStream() << ctr_totTx << ";" << simTime << ";" << id - 1 << ";" << (int) posTx.x << ";" << (int) posTx.y << ";" << packet_time_ms << ";" << p.second << std::endl; //for tx log

    }
    //    std::cout << p.first << " " << p.second << "\n";

    //    return p;

}

static void
ReceivePacket(Ptr<Socket> socket) {
    Ptr<Node> node = socket->GetNode();
    Ptr<MobilityModel> posMobility = node->GetObject<MobilityModel>();
    Vector posRx = posMobility->GetPosition();
    Ptr<Packet> packet = socket->Recv();
    uint8_t *buffer = new uint8_t[packet->GetSize()];
    packet->CopyData(buffer, packet->GetSize());
    std::string s = std::string((char*) buffer);

    size_t pos = 0;
    std::string copy = s;
    std::string token;
    int posTx_x;
    int posTx_y;
    for (int i = 0; i < 3; i++) {
        if (copy.find(";") != std::string::npos) {
            pos = copy.find(";");
            token = copy.substr(0, pos);
            if (i == 2) {
                posTx_x = atoi(token.c_str());
            }
            copy.erase(0, pos + 1);
        }
    }
    posTx_y = atoi(copy.c_str());

    double distance = sqrt(pow((posTx_x - posRx.x), 2.0) + pow((posTx_y - posRx.y), 2.0));
    if (distance <= baseline) {
        int id = node->GetId();
        int simTime = Simulator::Now().GetMicroSeconds();
        ctr_totRx++;
        *log_rx_data->GetStream() << ctr_totRx << ";" << simTime << ";" << id - 1 << ";" << s << std::endl;
    }
}

int
main(int argc, char *argv[]) {


    const clock_t begin_time = clock();

//    LogComponentEnable("v2x_communication_mode_4", LOG_INFO);
    //    LogComponentEnable("LteUePhy", LOG_INFO);
    //    LogComponentEnable("LteUeMac", LOG_INFO);
    //    LogComponentEnable("LteRlcAm", LOG_INFO);

    //    return 0;
    //    LogComponentEnable ("v2x_communication_mode_4", LOG_FUNCTION);
    // Initialize some values
    // NOTE: commandline parser is currently (05.04.2019) not working for uint8_t (Bug 2916)
    //

    // some parameters are defined in ETSI TS 103 613
    uint16_t simTime = 5; // Simulation time in seconds
    uint32_t numVeh = 3; // Number of vehicles    std::queue<packet_queue_item> time_cam_vec = generate_cam_sequence(2, simTime);

    lenCam = 190; // Length of CAM message in bytes [50-300 Bytes]
    double ueTxPower = 23.0; // Transmission power in dBm
    double probResourceKeep = 0.0; // Probability to select the previous resource again [0.0-0.8]
    uint32_t mcs = 20; // Modulation and Coding Scheme
    bool harqEnabled = false; // Retransmission enabled 
    bool adjacencyPscchPssch = true; // Subchannelization scheme
    bool partialSensing = false; // Partial sensing enabled (actual only partialSensing is false supported)
    uint16_t sizeSubchannel = 10; // Number of RBs per subchannel
    uint16_t numSubchannel = 5; // Number of subchannels per subframe
    uint16_t startRbSubchannel = 0; // Index of first RB corresponding to subchannelization
    uint16_t pRsvp = 100; // Resource reservation interval 
    uint16_t t1 = 4; // T1 value of selection window
    uint16_t t2 = 50; // T2 value of selection window
    uint16_t slBandwidth; // Sidelink bandwidth




    uint16_t CAMPacketInterval = 500; // CAM Packet interval in milliseconds

    bool denmEnabled = false; // set this to true if need to generate DENM packets with in Poisson distributed packet intervals
    uint16_t DENMPacketRepetitions = 5; // Number of DENM Packet repetitions for reliability(This number counts with the fist message = total number of similar messages)
    uint16_t DENMPacketRepInterval = 100; // DENM Packet repetitions interval in milliseconds
    double DENMlambda = 1; // lambda value(1/mean) in Poisson distribution of DENM generation periods



    std::string tracefile; // Name of the tracefile 


    // Command line arguments
    CommandLine cmd;
    cmd.AddValue("time", "Simulation Time", simTime);
    cmd.AddValue("numVeh", "Number of Vehicles", numVeh);
    cmd.AddValue("adjacencyPscchPssch", "Scheme for subchannelization", adjacencyPscchPssch);
    cmd.AddValue("sizeSubchannel", "Number of RBs per Subchannel", sizeSubchannel);
    cmd.AddValue("numSubchannel", "Number of Subchannels", numSubchannel);
    cmd.AddValue("startRbSubchannel", "Index of first subchannel index", startRbSubchannel);
    cmd.AddValue("T1", "T1 Value of Selection Window", t1);
    cmd.AddValue("T2", "T2 Value of Selection Window", t2);
    //cmd.AddValue ("harqEnabled", "HARQ Retransmission Enabled", harqEnabled);
    //cmd.AddValue ("partialSensingEnabled", "Partial Sensing Enabled", partialSensing);
    cmd.AddValue("lenCam", "Packetsize in Bytes", lenCam);
    cmd.AddValue("mcs", "Modulation and Coding Scheme", mcs);
    cmd.AddValue("pRsvp", "Resource Reservation Interval", pRsvp);
    cmd.AddValue("probResourceKeep", "Probability for selecting previous resource again", probResourceKeep);
    cmd.AddValue("log_simtime", "name of the simtime logfile", simtime);
    cmd.AddValue("log_rx_data", "name of the rx data logfile", rx_data);
    cmd.AddValue("log_tx_data", "name of the tx data logfile", tx_data);
    //    cmd.AddValue ("log_positions", "name of the tx data logfile", positions);
    //    cmd.AddValue ("log_connections", "name of the tx data logfile", connections);


    cmd.AddValue("tracefile", "Path of ns-3 tracefile", tracefile);
    cmd.AddValue("baseline", "Distance in which messages are transmitted and must be received", baseline);
    cmd.AddValue("max_queue_length", "Maximum length of TX queue", maxQueueLength);
    

    cmd.AddValue("cam_packet_interval", "CAM Packet interval in milliseconds", CAMPacketInterval);
    cmd.AddValue("denm_enabled", "if true: generate DENM packets with in Poisson distributed packet intervals", denmEnabled);
    cmd.AddValue("denm_repetitions_K", "Number of DENM Packet repetitions for reliability", DENMPacketRepetitions);
    cmd.AddValue("denm_rep_interval_T", "DENM Packet repetitions interval in milliseconds", DENMPacketRepInterval);
    cmd.AddValue("denm_lambda", "lambda value(1/mean) in Poisson distribution of DENM generation periods", DENMlambda);


    cmd.Parse(argc, argv);

    AsciiTraceHelper ascii;
    log_simtime = ascii.CreateFileStream(simtime);
    log_rx_data = ascii.CreateFileStream(rx_data);
    log_tx_data = ascii.CreateFileStream(tx_data);
    //    log_connections = ascii.CreateFileStream(connections);
    //    log_positions = ascii.CreateFileStream(positions); 




    NS_LOG_INFO("Starting network configuration...");

    // Set the UEs power in dBm
    Config::SetDefault("ns3::LteUePhy::TxPower", DoubleValue(ueTxPower));
    Config::SetDefault("ns3::LteUePhy::RsrpUeMeasThreshold", DoubleValue(-10.0));
    // Enable V2X communication on PHY layer
    Config::SetDefault("ns3::LteUePhy::EnableV2x", BooleanValue(true));

    // Set power
    Config::SetDefault("ns3::LteUePowerControl::Pcmax", DoubleValue(ueTxPower));
    Config::SetDefault("ns3::LteUePowerControl::PsschTxPower", DoubleValue(ueTxPower));
    Config::SetDefault("ns3::LteUePowerControl::PscchTxPower", DoubleValue(ueTxPower));

    if (adjacencyPscchPssch) {
        slBandwidth = sizeSubchannel * numSubchannel;
    } else {
        slBandwidth = (sizeSubchannel + 2) * numSubchannel;
    }

    // Configure for UE selected
    Config::SetDefault("ns3::LteUeMac::UlBandwidth", UintegerValue(slBandwidth));
    Config::SetDefault("ns3::LteUeMac::EnableV2xHarq", BooleanValue(harqEnabled));
    Config::SetDefault("ns3::LteUeMac::EnableAdjacencyPscchPssch", BooleanValue(adjacencyPscchPssch));
    Config::SetDefault("ns3::LteUeMac::EnablePartialSensing", BooleanValue(partialSensing));
    Config::SetDefault("ns3::LteUeMac::SlGrantMcs", UintegerValue(mcs));
    Config::SetDefault("ns3::LteUeMac::SlSubchannelSize", UintegerValue(sizeSubchannel));
    Config::SetDefault("ns3::LteUeMac::SlSubchannelNum", UintegerValue(numSubchannel));
    Config::SetDefault("ns3::LteUeMac::SlStartRbSubchannel", UintegerValue(startRbSubchannel));
    Config::SetDefault("ns3::LteUeMac::SlPrsvp", UintegerValue(pRsvp));
    Config::SetDefault("ns3::LteUeMac::SlProbResourceKeep", DoubleValue(probResourceKeep));
    Config::SetDefault("ns3::LteUeMac::SelectionWindowT1", UintegerValue(t1));
    Config::SetDefault("ns3::LteUeMac::SelectionWindowT2", UintegerValue(t2));
    //Config::SetDefault ("ns3::LteUeMac::EnableExcludeSubframe", BooleanValue(excludeSubframe)); 

    ConfigStore inputConfig;
    inputConfig.ConfigureDefaults();

    // Create node container to hold all UEs 
    NodeContainer ueAllNodes;


    NS_LOG_INFO("Num. Vehicles:" << numVeh << ", simTime:" << simTime);
    NS_LOG_INFO("Installing Mobility Model...");

    if (tracefile.empty()) {
        // Create nodes
        ueVeh.Create(numVeh);
        ueAllNodes.Add(ueVeh);

        // Install constant random positions 
        MobilityHelper mobVeh;
        mobVeh.SetMobilityModel("ns3::ConstantPositionMobilityModel");
        Ptr<ListPositionAllocator> staticVeh[ueVeh.GetN()];
        for (uint16_t i = 0; i < ueVeh.GetN(); i++) {
            staticVeh[i] = CreateObject<ListPositionAllocator>();
            Ptr<UniformRandomVariable> rand = CreateObject<UniformRandomVariable> ();
            int x = rand->GetValue(0, 100);
            int y = rand->GetValue(0, 100);
            double z = 1.5;
            staticVeh[i]->Add(Vector(x, y, z));
            mobVeh.SetPositionAllocator(staticVeh[i]);
            mobVeh.Install(ueVeh.Get(i));
        }
    } else {
        // Create nodes
        ueVeh.Create(numVeh);
        ueAllNodes.Add(ueVeh);

        Ns2MobilityHelper ns2 = Ns2MobilityHelper(tracefile);
        ns2.Install();
    }

    //    std::cout << CAMPacketInterval; //UintegerValue(CAMPacketInterval);
    //    return 0;

    NS_LOG_INFO("Genrating Packet sequences...");
    //packet queue properties
    //#define LIMIT 100 // Specifying the maximum limit of the queue
    //int packet_queue[LIMIT]; // Array implementation of queue
    //int queue_front, queue_rear; // To insert and delete the data elements in the queue respectively
    //void push_queue(); // Function used to insert an element into the queue
    //void pop_queue(); // Function used to delete an element from the queue
    //void display(); // Function used to display all the elements in the queue according to FIFO rule

    //std::array<std::pair<double,std::string>, 4> time_cam_arr{{0, "first cam packet"}, {500, "2nd cam packet"}, {1000, "3rd cam packet"}, {1500, "4th cam packet"}};
    //int CAM_queue[3][2] = {{0,1}, {500,10}, {1000,7}}; //array of {time,value} tuples
    //int DENM_queue[3][2] = {{0,1}, {0.6,73}, {1.5,6}};




    std::cout << "Denm enabled:" << denmEnabled << std::endl;




    //    uint16_t sim_duration_s = 10;
    //    std::queue<packet_queue_item> time_denm_queue = generate_denm_sequence(repetition_interval_ms, K, lambda, sim_duration_s);
    //    std::queue<packet_queue_item> time_cam_queue = generate_cam_sequence(CAMPacketInterval, simTime);

    //    std::queue<packet_queue_item> all_packet_queues = merge_queues(time_denm_queue, time_cam_queue);
    //    print_packet_seq(all_packet_queues);

    //here for all nodes CAM sequene is similar. if different then put generation part inside the loop
    //    std::queue<packet_queue_item> time_cam_queue = generate_cam_sequence(CAMPacketInterval, simTime);
    for (uint32_t u = 0; u < ueAllNodes.GetN(); ++u) {
        std::queue<packet_queue_item> time_cam_queue = generate_cam_sequence(CAMPacketInterval, simTime);
        //        cam_packet_map[u] = time_cam_queue;

        if (denmEnabled) {
            std::queue<packet_queue_item> time_denm_queue = generate_denm_sequence(DENMPacketRepInterval, DENMPacketRepetitions, DENMlambda, simTime);
            packet_map[u] = merge_queues(time_denm_queue, time_cam_queue);
            //            denm_packet_map[u] = time_denm_queue;
            //                    std::cout << "with denm" << std::endl;
        } else {
            packet_map[u] = time_cam_queue;
        }
    }


    //    std::cout << "All packets lists to be sent:" << std::endl;
    print_packet_map(packet_map);
    //    print_packet_map(denm_packet_map);

    //    return 0;

    //here for all nodes CAM sequene is similar. if different then put generation part inside the loop
    //    std::queue<packet_queue_item> time_cam_queue = generate_cam_sequence(CAMPacketInterval, simTime);
    //    for (uint32_t u = 0; u < ueAllNodes.GetN(); ++u) {
    //        cam_packet_map[u] = time_cam_queue;
    //    }

    //    print_packet_map(cam_packet_map);


    //    queue_get_first(&cam_packet_map[1]);
    //    print_packet_seq(cam_packet_vector[2]);

    //    print_packet_map(cam_packet_map);




    NS_LOG_INFO("Creating helpers...");
    // EPC
    Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper>();
    Ptr<Node> pgw = epcHelper->GetPgwNode();

    // LTE Helper
    Ptr<LteHelper> lteHelper = CreateObject<LteHelper>();
    lteHelper->SetEpcHelper(epcHelper);
    lteHelper->DisableNewEnbPhy(); // Disable eNBs for out-of-coverage modelling

    // V2X 
    Ptr<LteV2xHelper> lteV2xHelper = CreateObject<LteV2xHelper> ();
    lteV2xHelper->SetLteHelper(lteHelper);

    // Configure eNBs' antenna parameters before deploying them.
    lteHelper->SetEnbAntennaModelType("ns3::NistParabolic3dAntennaModel");

    // Set pathloss model
    // FIXME: InstallEnbDevice overrides PathlossModel Frequency with values from Earfcn
    // 
    lteHelper->SetAttribute("UseSameUlDlPropagationCondition", BooleanValue(true));
    Config::SetDefault("ns3::LteEnbNetDevice::UlEarfcn", StringValue("54990"));
    //Config::SetDefault ("ns3::CniUrbanmicrocellPropagationLossModel::Frequency", DoubleValue(5800e6));
    lteHelper->SetAttribute("PathlossModel", StringValue("ns3::CniUrbanmicrocellPropagationLossModel"));


    // Create eNB Container
    NodeContainer eNodeB;
    eNodeB.Create(1);

    // Topology eNodeB
    Ptr<ListPositionAllocator> pos_eNB = CreateObject<ListPositionAllocator>();
    pos_eNB->Add(Vector(5, -10, 30));

    //  Install mobility eNodeB
    MobilityHelper mob_eNB;
    mob_eNB.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mob_eNB.SetPositionAllocator(pos_eNB);
    mob_eNB.Install(eNodeB);

    // Install Service
    NetDeviceContainer enbDevs = lteHelper->InstallEnbDevice(eNodeB);

    // Required to use NIST 3GPP model
    BuildingsHelper::Install(eNodeB);
    BuildingsHelper::Install(ueAllNodes);
    BuildingsHelper::MakeMobilityModelConsistent();

    // Install LTE devices to all UEs 
    NS_LOG_INFO("Installing UE's network devices...");
    lteHelper->SetAttribute("UseSidelink", BooleanValue(true));
    NetDeviceContainer ueRespondersDevs = lteHelper->InstallUeDevice(ueVeh);
    NetDeviceContainer ueDevs;
    ueDevs.Add(ueRespondersDevs);

    // Install the IP stack on the UEs
    NS_LOG_INFO("Installing IP stack...");
    InternetStackHelper internet;
    internet.Install(ueAllNodes);

    // Assign IP adress to UEs
    NS_LOG_INFO("Allocating IP addresses and setting up network route...");
    Ipv4InterfaceContainer ueIpIface;
    ueIpIface = epcHelper->AssignUeIpv4Address(ueDevs);
    Ipv4StaticRoutingHelper Ipv4RoutingHelper;

    for (uint32_t u = 0; u < ueAllNodes.GetN(); ++u) {
        Ptr<Node> ueNode = ueAllNodes.Get(u);
        // Set the default gateway for the UE
        Ptr<Ipv4StaticRouting> ueStaticRouting = Ipv4RoutingHelper.GetStaticRouting(ueNode->GetObject<Ipv4>());
        ueStaticRouting->SetDefaultRoute(epcHelper->GetUeDefaultGatewayAddress(), 1);
    }

    NS_LOG_INFO("Attaching UE's to LTE network...");
    //Attach each UE to the best available eNB
    lteHelper->Attach(ueDevs);

    NS_LOG_INFO("Creating sidelink groups...");
    std::vector<NetDeviceContainer> txGroups;
    txGroups = lteV2xHelper->AssociateForV2xBroadcast(ueRespondersDevs, numVeh);

    lteV2xHelper->PrintGroups(txGroups);
    // compute average number of receivers associated per transmitter and vice versa
    double totalRxs = 0;
    std::map<uint32_t, uint32_t> txPerUeMap;
    std::map<uint32_t, uint32_t> groupsPerUe;

    std::vector<NetDeviceContainer>::iterator gIt;
    for (gIt = txGroups.begin(); gIt != txGroups.end(); gIt++) {
        uint32_t numDevs = gIt->GetN();

        totalRxs += numDevs - 1;
        uint32_t nId;

        for (uint32_t i = 1; i < numDevs; i++) {
            nId = gIt->Get(i)->GetNode()->GetId();
            txPerUeMap[nId]++;
        }
    }

    double totalTxPerUe = 0;
    std::map<uint32_t, uint32_t>::iterator mIt;
    for (mIt = txPerUeMap.begin(); mIt != txPerUeMap.end(); mIt++) {
        totalTxPerUe += mIt->second;
        groupsPerUe [mIt->second]++;
    }

    //    lteV2xHelper->PrintGroups (txGroups, log_connections);

    NS_LOG_INFO("Installing applications...");

    // Application Setup for Responders
    std::vector<uint32_t> groupL2Addresses;
    uint32_t groupL2Address = 0x00;
    Ipv4AddressGenerator::Init(Ipv4Address("225.0.0.0"), Ipv4Mask("255.0.0.0"));
    Ipv4Address clientRespondersAddress = Ipv4AddressGenerator::NextAddress(Ipv4Mask("255.0.0.0"));

    uint16_t application_port = 8000; // Application port to TX/RX
    NetDeviceContainer activeTxUes;

    // Set Sidelink V2X Traces
    /*AsciiTraceHelper ascii;
    Ptr<OutputStreamWrapper> stream = ascii.CreateFileStream ("sidelinkV2x_out_monitoring.tr");
     *stream->GetStream () << "Time" << std::endl;
    
    AsciiTraceHelper ascii1;
    Ptr<OutputStreamWrapper> stream1 = ascii1.CreateFileStream ("sidelinkV2x_out_announcement_phy.tr");
     *stream1->GetStream () << "Time" << std::endl;
    
    AsciiTraceHelper ascii2;
    Ptr<OutputStreamWrapper> stream2 = ascii1.CreateFileStream ("sidelinkV2x_out_announcement_mac.tr");
     *stream2->GetStream () << "Time" << std::endl;

    std::ostringstream oss;
    oss.str("");*/

    for (gIt = txGroups.begin(); gIt != txGroups.end(); gIt++) {
        // Create Sidelink bearers
        // Use Tx for the group transmitter and Rx for all the receivers
        // Split Tx/Rx

        NetDeviceContainer txUe((*gIt).Get(0));
        activeTxUes.Add(txUe);
        NetDeviceContainer rxUes = lteV2xHelper->RemoveNetDevice((*gIt), txUe.Get(0));
        Ptr<LteSlTft> tft = Create<LteSlTft> (LteSlTft::TRANSMIT, clientRespondersAddress, groupL2Address);
        lteV2xHelper->ActivateSidelinkBearer(Seconds(0.0), txUe, tft);
        tft = Create<LteSlTft> (LteSlTft::RECEIVE, clientRespondersAddress, groupL2Address);
        lteV2xHelper->ActivateSidelinkBearer(Seconds(0.0), rxUes, tft);

        //std::cout << "Created group L2Address=" << groupL2Address << " IPAddress=";
        //clientRespondersAddress.Print(std::cout);
        //std::cout << std::endl;

        //Individual Socket Traffic Broadcast everyone
        Ptr<Socket> host = Socket::CreateSocket(txUe.Get(0)->GetNode(), TypeId::LookupByName("ns3::UdpSocketFactory"));
        host->Bind();
        host->Connect(InetSocketAddress(clientRespondersAddress, application_port));
        host->SetAllowBroadcast(true);
        host->ShutdownRecv();

        //Ptr<LteUeRrc> ueRrc = DynamicCast<LteUeRrc>( txUe.Get (0)->GetObject<LteUeNetDevice> ()->GetRrc () );    
        //ueRrc->TraceConnectWithoutContext ("SidelinkV2xMonitoring", MakeBoundCallback (&SidelinkV2xMonitoringTrace, stream));
        //oss << txUe.Get(0) ->GetObject<LteUeNetDevice>()->GetImsi(); 
        //Ptr<LteUePhy> uePhy = DynamicCast<LteUePhy>( txUe.Get (0)->GetObject<LteUeNetDevice> ()->GetPhy () );
        //uePhy->TraceConnect ("SidelinkV2xAnnouncement", oss.str() ,MakeBoundCallback (&SidelinkV2xAnnouncementPhyTrace, stream1));
        //uePhy->TraceConnectWithoutContext ("SidelinkV2xAnnouncement", MakeBoundCallback (&SidelinkV2xAnnouncementPhyTrace, host));
        Ptr<LteUeMac> ueMac = DynamicCast<LteUeMac>(txUe.Get(0)->GetObject<LteUeNetDevice> ()->GetMac());
        ueMac->TraceConnectWithoutContext("SidelinkV2xAnnouncement", MakeBoundCallback(&SidelinkV2xAnnouncementMacTrace, host));
        //ueMac->TraceConnect ("SidelinkV2xAnnouncement", oss.str() ,MakeBoundCallback (&SidelinkV2xAnnouncementMacTrace, stream2));

        Ptr<Socket> sink = Socket::CreateSocket(txUe.Get(0)->GetNode(), TypeId::LookupByName("ns3::UdpSocketFactory"));
        sink->Bind(InetSocketAddress(Ipv4Address::GetAny(), application_port));
        sink->SetRecvCallback(MakeCallback(&ReceivePacket));

        //store and increment addresses
        groupL2Addresses.push_back(groupL2Address);
        groupL2Address++;
        clientRespondersAddress = Ipv4AddressGenerator::NextAddress(Ipv4Mask("255.0.0.0"));
    }

    NS_LOG_INFO("Creating Sidelink Configuration...");
    Ptr<LteUeRrcSl> ueSidelinkConfiguration = CreateObject<LteUeRrcSl>();
    ueSidelinkConfiguration->SetSlEnabled(true);
    ueSidelinkConfiguration->SetV2xEnabled(true);

    LteRrcSap::SlV2xPreconfiguration preconfiguration;
    preconfiguration.v2xPreconfigFreqList.freq[0].v2xCommPreconfigGeneral.carrierFreq = 54890;
    preconfiguration.v2xPreconfigFreqList.freq[0].v2xCommPreconfigGeneral.slBandwidth = slBandwidth;

    preconfiguration.v2xPreconfigFreqList.freq[0].v2xCommTxPoolList.nbPools = 1;
    preconfiguration.v2xPreconfigFreqList.freq[0].v2xCommRxPoolList.nbPools = 1;

    //v2xSchedulingPool
    SlV2xPreconfigPoolFactory pFactory;
    pFactory.SetHaveUeSelectedResourceConfig(true);
    pFactory.SetSlSubframe(std::bitset<20> (0xFFFFF));
    pFactory.SetAdjacencyPscchPssch(adjacencyPscchPssch);
    pFactory.SetSizeSubchannel(sizeSubchannel);
    pFactory.SetNumSubchannel(numSubchannel);
    pFactory.SetStartRbSubchannel(startRbSubchannel);
    pFactory.SetStartRbPscchPool(0);
    pFactory.SetDataTxP0(-4);
    pFactory.SetDataTxAlpha(0.9);


    preconfiguration.v2xPreconfigFreqList.freq[0].v2xCommTxPoolList.pools[0] = pFactory.CreatePool();
    preconfiguration.v2xPreconfigFreqList.freq[0].v2xCommRxPoolList.pools[0] = pFactory.CreatePool();
    ueSidelinkConfiguration->SetSlV2xPreconfiguration(preconfiguration);

    // Print Configuration
    *log_rx_data->GetStream() << "RxPackets;RxTime;RxId;TxId;TxTime;xPos;yPos;genTime;pkt_info" << std::endl;
    *log_tx_data->GetStream() << "TxPackets;TxTime;TxId;xPos;yPos;genTime;pkt_info" << std::endl;

    NS_LOG_INFO("Installing Sidelink Configuration...");
    lteHelper->InstallSidelinkV2xConfiguration(ueRespondersDevs, ueSidelinkConfiguration);

    NS_LOG_INFO("Enabling LTE traces...");
    lteHelper->EnableTraces();

    *log_simtime->GetStream() << "Simtime;TotalRx;TotalTx;PRR" << std::endl;
    Simulator::Schedule(Seconds(1), &PrintStatus, 1, log_simtime);
    NS_LOG_INFO("Starting Simulation...");
    Simulator::Stop(MilliSeconds(simTime * 1000 + 40));
    Simulator::Run();
    Simulator::Destroy();
    std::cout << "not sent packets lists:" << std::endl;
    std::cout << "All Packet queues:" << std::endl;
    //    print_packet_map(cam_packet_map);
    //    print_packet_map(denm_packet_map);
    print_packet_map(packet_map);
    std::cout << "Final dynamic queues:" << std::endl;
    print_packet_map(dynamic_pkt_map);
    //    uint32_t end_time = time(NULL);
    //    std::cout << clock() << std::endl;
    //    std::cout << start_time << std::endl;
    //    std::cout << end_time << std::endl;

    
    std::cout << "generated=" << ctr_all_generated_packets << " Packets." << std::endl;
    std::cout << "dropped due to queue limit=" << ctr_qdrop<< " Packets." << std::endl;
    std::cout << "Transmitted to PHY=" << ctr_totTx<< " Packets." << std::endl;
    std::cout << "Recieved=" << ctr_totRx<< " Packets." << std::endl;
    
    
    std::cout << "generated,dropped due to queue limit,Transmitted to PHY,Recieved" << std::endl;
    std::cout << "CSV: "<< ctr_all_generated_packets <<","<< ctr_qdrop<<","<< ctr_totTx<<","<< ctr_totRx<< std::endl;
    
    std::cout << "t=" << clock() << "\t Rx/Tx=" << ctr_totRx << "/" << ctr_totTx << "\t PRR=" << (double) ctr_totRx / ctr_totTx << std::endl;
    std::cout << "t=" << clock() << "\t Rx/Tx=" << ctr_totRx << "/" << ctr_all_generated_packets << "\t PRR=" << (double) ctr_totRx / ctr_all_generated_packets << std::endl;
    *log_simtime->GetStream() << clock() << ";" << ctr_totRx << ";" << ctr_all_generated_packets << ";" << (double) ctr_totRx / ctr_all_generated_packets << std::endl;



    
    
    std::cout << "Time Spent: " << float( clock() - begin_time) / CLOCKS_PER_SEC << " Seconds" << std::endl;
    NS_LOG_INFO("Simulation done.");
    return 0;
}




//copied from 80211 code before remove queue
//
//
////defining a format for ITS packet in simulation code
//// 8 bit unsigned integer for packet type and a string for represent packet content
////packet types:
////HPD:0, DENM:1, CAM:2, MHD:3
//
////packet type, packet
//typedef std::pair<uint8_t, Ptr<Packet>> its_packet;
//
////time, its_packet
//typedef std::pair<uint32_t, its_packet> packet_queue_item;
//
////defining the packet queue: for each node, simulator has to create a packet queue at the beginning for entire simulator time
////A standard queue of packet queue items defined above
//typedef std::queue<packet_queue_item> packet_queue;
//
////initializing all packet queues for all nodes: a standard map of nodeid:packet_queue pairs
////16 bit unsigned int for node id and the packet queue defined above
//std::map<uint16_t, packet_queue> packet_map;
//
///**
//   * \brief Create a CAM or DENM packet queue
//   *
//   * The input data is copied: the input
//   * buffer is untouched.
//   *
//   * \param sim_duration_s similuation running duration in seconds
//   * \param packet_type Type of the packet HPD:0, DENM:1, CAM:2, MHD:3
//   * \param T interval between two repeted packets in milliseconds 
//   * \param K number of times sending the same paket for increase the reliability (this K includes the fist original packet)
//   * \param lambda 1/mean of exponential distribution of time periods between two DENM generation incidents
//   */
//packet_queue
//generate_packet_queue (uint32_t sim_duration_s, uint8_t packet_type, uint32_t T, uint8_t K,
//                       double lambda)
//{
//  packet_length = 100;
//  uint32_t time_ms = 0.0;
//  packet_queue packet_queue;
//
//  if (packet_type == 2) //CAM packet
//    { //CAM packet
//      while (time_ms < sim_duration_s * 1000.0)
//        {
//          while (time_ms < sim_duration_s * 1000.0)
//            {
//              Ptr<Packet> packet_ptr = Create<Packet> (100);
//              its_packet packet = {packet_type, packet_ptr};
//              packet_queue_item queue_item = {time_ms, packet};
//              packet_queue.push (queue_item);
//
//              time_ms += T;
//            }
//        };
//    }
//  else
//    {
//      double bound = 0;
//      double mean = double (1.0 / lambda);
//      uint16_t random_value; //distributed exponentially with lambda (SD=Mean=1/lambda)
//      uint16_t packet_id = 1;
//
//      //randomize with time to generate different sequences in different iterations
//      ns3::RngSeedManager::SetSeed (time (NULL));
//
//      Ptr<ExponentialRandomVariable> x_H = CreateObject<ExponentialRandomVariable> ();
//      x_H->SetAttribute ("Mean", DoubleValue (mean));
//      x_H->SetAttribute ("Bound", DoubleValue (bound));
//      // NS_LOG_INFO("mean " << mean);
//      while (time_ms < sim_duration_s * 1000.0)
//        {
//          random_value = x_H->GetValue () * 1000;
//          time_ms += random_value;
//          Ptr<Packet> packet_ptr = Create<Packet> (100);
//          its_packet packet = {packet_type, packet_ptr};
//          packet_queue_item queue_item = {time_ms, packet};
//          packet_queue.push (queue_item);
//
//          //repeating:
//          //itr_k is started from 1 because already sent the original packet. Thease are duplicates for reliability
//          for (uint8_t itr_k = 1; itr_k < K; itr_k++)
//            {
//              time_ms += T;
//              Ptr<Packet> packet_ptr = Create<Packet> (100);
//              its_packet packet = {packet_type, packet_ptr};
//              packet_queue_item queue_item = {time_ms, packet};
//              packet_queue.push (queue_item);
//            }
//        };
//    }
//  return packet_queue;
//}


